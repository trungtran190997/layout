import { createStackNavigator } from "react-navigation";
import { Login, EditName, ForgetPassword } from "@Screens";
import getSlideFromRightTransitionConfig from "./transitionConfig";
import { Strings } from "@Localization";
const AuthenticationStack = createStackNavigator(
  {
    Login: Login,
    ForgetPassword
  },
  {
    transitionConfig: getSlideFromRightTransitionConfig,
    initialRouteName: "Login",
    defaultNavigationOptions: {
      header: null
    }
  }
);

export default AuthenticationStack;
