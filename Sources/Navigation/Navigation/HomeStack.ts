import { createStackNavigator } from "react-navigation";
import { Home, HomeScreen, Homepage1 } from "@Screens";

import getSlideFromRightTransitionConfig from "./transitionConfig";
// import Homepage1 from "Sources/Screens/Home/Homepage1";

const HomeStack = createStackNavigator(
  {
    // Home: Home,
    HomeScreen: HomeScreen,
    Homepage1: Homepage1
  },
  {
    transitionConfig: getSlideFromRightTransitionConfig,
    initialRouteName: "HomeScreen",
    defaultNavigationOptions: ({ navigation }) => {
      const gesturesEnabled = navigation.getParam("swipeBackEnabled", true);
      return {
        header: null,
        gesturesEnabled
      };
    },
    navigationOptions: ({ navigation }) => {
      const params =
        navigation.state.routes[navigation.state.index].params || {};
      return {
        drawerLockMode: params.drawerLockMode
      };
    }
  }
);

export default HomeStack;
