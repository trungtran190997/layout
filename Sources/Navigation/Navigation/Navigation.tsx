import {
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator
} from "react-navigation";
import { Start } from "@Screens";
import AuthenticationStack from "./AuthenticationStack";
import HomeStack from "./HomeStack";
import getSlideFromRightTransitionConfig from "./transitionConfig";
import { AvaiableFacility, Payment, Promotion, Icon } from "@Components";
import { Strings } from "@Localization";
import React from "react";
import {} from "react-native";
import Assest from "@Assets";
import { Navigator } from "@Navigation";

const strings = {
  home: Strings.TabBar.home,
  facility: Strings.TabBar.facility,
  payment: Strings.TabBar.payment
};
const RootStack = createStackNavigator(
  {
    Authentication: AuthenticationStack,
    Home: HomeStack,
    Start: Start
  },
  {
    initialRouteName: "Start",
    transitionConfig: getSlideFromRightTransitionConfig,
    headerMode: "none",
    defaultNavigationOptions: {
      swipeEnabled: false,
      gesturesEnabled: false,
      header: null
    }
  }
);

const tabBar = createBottomTabNavigator(
  {
    RootStack: {
      screen: RootStack,
      navigationOptions: {
        tabBarLabel: strings.home,
        tabBarIcon: <Icon icon={Assest.images.iconHome} />
        // tabBarOnPress: {
        //   tabBarIcon: <Icon icon={Assest.images.iconHomeActive} />
        // }
      }
    },
    AvaiableFacility: {
      screen: AvaiableFacility,
      navigationOptions: {
        tabBarLabel: strings.facility,
        tabBarIcon: <Icon icon={Assest.images.iconFacility} />
        // tabBarOnPress: {}
      }
    },
    Payment: {
      screen: Payment,
      navigationOptions: {
        tabBarLabel: strings.payment,
        tabBarIcon: <Icon icon={Assest.images.iconPayment} />
        // tabBarOnPress: {
        //   tabBarIcon: <Icon icon={Assest.images.iconPaymentActive} />
        // }
      }
    }
  },
  {
    tabBarOptions: {
      // activeI: <Icon icon={Assest.images.iconFacility} />
      activeTintColor: "#91C95B"
    }
  }
);

const AppContainer = createAppContainer(tabBar);

export default AppContainer;
