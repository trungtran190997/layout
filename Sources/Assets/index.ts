const images = {
  splashScreen: require("../Assets/images/Splashscreen.png"),
  buttonNext: require("../Assets/images/button1.png"),
  logo: require("../Assets/images/Group5211.png"),
  checked: require("../Assets/images/checked.png"),
  backgroundHome: require("../Assets/images/Rectangle711.png"),
  nenxanh: require("../Assets/images/nenxanh.png"),
  hamberger: require("../Assets/images/hamberger.png"),
  sun: require("../Assets/images/sun.png"),
  group1: require("../Assets/images/Group1.png"),
  group2: require("../Assets/images/Group2.png"),
  bell: require("../Assets/images/bell.png"),
  book: require("../Assets/images/book.png"),
  imgTitle: require("../Assets/images/imgTitle.png"),
  car: require("../Assets/images/car.png"),
  swimming: require("../Assets/images/swimming.png"),
  BBQ: require("../Assets/images/BBQ.png"),
  next: require("../Assets/images/next.png"),
  line: require("../Assets/images/line.png"),
  dayLeft: require("../Assets/images/dayLeft.png"),
  payNow: require("../Assets/images/payNow.png"),
  backgroundSaleOff: require("@Assets/images/backgroundSaleOff.png"),
  bgVailid: require("@Assets/images/bgVailid.png"),
  iconHome: require("@Assets/images/iconHome.png"),
  iconPayment: require("@Assets/images/iconPayment.png"),
  iconFacility: require("@Assets/images/iconFacility.png"),
  iconHomeActive: require("@Assets/images/iconHomeActive.png"),
  iconPaymentActive: require("@Assets/images/iconPaymentActive.png"),
  iconFacilityActive: require("@Assets/images/iconFacilityActive.png")
};

const colors = {
  primary: "#5b8929",
  primaryDark: "#429321"
};

const font = {
  avenir: {}
};

export default {
  images,
  colors,
  font
};
