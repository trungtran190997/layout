import React from "react";
import {
  ImageBackground,
  StyleSheet,
  Dimensions,
  Image,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { Col, Row } from "rn-components";
import Assets from "@Assets";
import { Strings } from "@Localization";
import { AvaiableFacility, Payment, Promotion } from "@Components";
import { Navigator } from "@Navigation";
import { NavigationEvents } from "react-navigation";

const { width, height } = Dimensions.get("screen");

type Props = {};
class Homepage1 extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    // Navigator.setTabbar("Homepage1", true);
  }
  render() {
    return (
      <Col flex={1} style={styles.greyBackground}>
        <NavigationEvents
          onWillFocus={payload => console.log("will focus", payload)}
          onDidFocus={payload => console.log("did focus", payload)}
          onWillBlur={payload => console.log("will blur", payload)}
          onDidBlur={payload => console.log("did blur", payload)}
        />
        <Col flex={4 / 8} style={styles.whiteBackground}>
          <Col style={styles.nenxanh} />
          <Col flex={1} style={styles.container}>
            <Col flex={2} alignVertical="center">
              <Row flex={1 / 3} alignHorizontal="space-between">
                <Image source={Assets.images.hamberger} />
                <Image source={Assets.images.bell} />
              </Row>
              <Col flex={2 / 3} alignVertical="center">
                <Row
                  alignHorizontal="space-between"
                  alignVertical="center"
                  flex={2}
                >
                  <Text style={styles.textWellcome}>
                    {Strings.Homepage1.wellcome}
                  </Text>
                  <Row>
                    <Text style={styles.textDay}>{Strings.Homepage1.fri}</Text>
                    <Image source={Assets.images.sun} />
                  </Row>
                </Row>
                <Text style={styles.textGood}>
                  {Strings.Homepage1.goodMorning}
                </Text>
              </Col>
            </Col>
            <Col
              flex={2}
              style={styles.bookNow}
              alignHorizontal="center"
              alignVertical="center"
            >
              <TouchableOpacity>
                <Image source={Assets.images.book} />
              </TouchableOpacity>
              <Text style={styles.textBook}>{Strings.Homepage1.bookNow}</Text>
            </Col>
          </Col>
        </Col>
        <Col flex={4 / 8}>
          <ScrollView>
            <AvaiableFacility />
            <Payment />
            <Promotion />
          </ScrollView>
        </Col>
      </Col>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: width / 20
  },
  greyBackground: {
    backgroundColor: "#ECECEC"
  },

  whiteBackground: {
    backgroundColor: "white"
  },
  separator: {
    height: 6,
    backgroundColor: "grey",
    width
  },
  nenxanh: {
    backgroundColor: "#91C95B",
    width,
    height: height / 3,
    position: "absolute"
  },
  bookNow: {
    backgroundColor: "white",
    borderColor: "#006FE3",
    borderWidth: 0.5,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  textBook: {
    color: "#FE4F70",
    fontSize: 17
  },
  textGood: {
    fontSize: 13,
    flex: 1,
    color: "white"
  },
  textWellcome: {
    fontSize: 28,
    fontWeight: "bold",
    color: "white"
  },
  textDay: {
    fontSize: 12,
    color: "white",
    fontWeight: "700",
    marginRight: 7
  }
});

export default Homepage1;
