import React from "react";
import { View, StyleSheet, ImageBackground, Image } from "react-native";
import Assets from "@Assets";
import { Col, Text } from "rn-components";
import { Strings } from "@Localization";
import { types } from "@babel/core";
import { Navigator } from "@Navigation";
// import Navigation from 'react-navigation'
type Props = {
  navigation: any;
};

class HomeScreen extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }
  componentDidMount() {
    setTimeout(() => Navigator.navTo("Homepage1"), 2000);
  }
  render() {
    return (
      <ImageBackground
        source={Assets.images.backgroundHome}
        style={styles.container}
        resizeMode="stretch"
      >
        <Col
          flex={1}
          alignSelf="center"
          alignVertical="center"
          alignHorizontal="center"
        >
          <Image
            source={Assets.images.logo}
            style={styles.logo}
            resizeMethod="auto"
          />
          <Text style={styles.hi}>
            {Strings.Home.hi}
            {this.props.navigation.getParam("email", "My Friend!")}
          </Text>
          <Text style={styles.wellcome}>{Strings.Home.wellcome}</Text>
        </Col>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // width: undefined,
    // height: undefined,
    flex: 1
  },
  logo: {},
  hi: {
    fontSize: 28,
    color: "#213654"
  },
  wellcome: {
    color: "#213654",
    fontSize: 28
  }
});

export default HomeScreen;
