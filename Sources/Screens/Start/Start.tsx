import React from "react";
import { View, Image, Dimensions, ImageBackground } from "react-native";
import { Navigator } from "@Navigation";
import { Authentication } from "@Services";
import RNSplashScreen from "react-native-splash-screen";
import { StyleSheet, Text } from "rn-components";
import Assets from "@Assets";
// import img from "@Assets/images/Splashscreen.png";

const { width, height } = Dimensions.get("screen");
class Start extends React.Component {
  private timeoutHandler: any;

  private start = () => {
    RNSplashScreen.hide();
    Authentication.createSession()
      .then(() => {
        this.timeoutHandler = setTimeout(() => Navigator.navTo("Home"), 500);
      })
      .catch(() => {
        this.timeoutHandler = setTimeout(
          () => Navigator.navTo("Homepage1"),
          500
        );
      });
  };

  componentWillMount() {
    this.start();
  }

  componentWillUnmount() {
    if (this.timeoutHandler) {
      clearTimeout(this.timeoutHandler);
      this.timeoutHandler = null;
    }
  }

  render() {
    return (
      // <Image
      //   source={Assets.images.splashScreen}
      //   style={styles.container}
      //   resizeMode="stretch"
      // />
      <ImageBackground
        source={Assets.images.splashScreen}
        style={styles.container}
        resizeMode="stretch"
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: "stretch",
    width: undefined,
    height: undefined
  }
});
export default Start;
