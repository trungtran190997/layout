import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { Col, Row, TextInput } from "rn-components";
import { Strings } from "@Localization";
import { type } from "os";

type State = {
  newName: string;
};
type Props = {};
class EditName extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      newName: ""
    };
  }
  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Col flex={1} style={{ marginLeft: 15 }}>
          <Text style={styles.titleText}>{Strings.EditName.titleOldName}</Text>
          <Text style={styles.text}>{Strings.EditName.oldName}</Text>
          <Text style={[styles.titleText, { marginTop: 20 }]}>
            {Strings.EditName.titleNewName}
          </Text>
          <Row style={styles.underline}>
            <TextInput
              style={styles.hideBorder}
              placeholder="lksjdlfjsdlkj"
              value={this.state.newName}
              onChangeText={newName => {
                this.setState({ newName });
              }}
            />
          </Row>
        </Col>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  underline: {
    borderBottomWidth: 1,
    borderBottomColor: "black"
  },
  hideBorder: {
    borderWidth: 0,
    flex: 1,
    fontWeight: "bold",
    fontSize: 35
  },
  titleText: {
    fontSize: 25
  },
  text: {
    fontSize: 35,
    fontWeight: "bold"
  }
});

export default EditName;
