import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
// import { Navigator } from "@Navigation";
// import { Header, Button } from "rn-components";
import { Navigator } from "@Navigation";
import { Col, Row, TextInput } from "rn-components";
import { Strings } from "@Localization";
import Assets from "@Assets";
import { HomeScreen } from "@Screens";
import Navigation from "react-navigation";
import { Authentication } from "@Services";
// import { State } from "react-native-gesture-handler";
const { width, height } = Dimensions.get("screen");
type State = {
  email: string;
  showCheck: boolean;
};
type Props = {
  Navigation: any;
};
class Login extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      email: "",
      showCheck: false
    };
  }
  private onLogin = () => {
    Navigator.showLoading();
    setTimeout(
      () => [
        Authentication.loginAndCreateSession("123", "123")
          .then(() => {
            Navigator.navTo("Home", { email: this.state.email }),
              Navigator.hideLoading();
          })
          .catch(err => {
            Navigator.navTo("Home", { email: this.state.email }),
              Navigator.hideLoading();
          })
      ],
      500
    );
    // Alert.alert();
  };
  private onForgetPassword = () => {
    // Navigator.showLoading();
    setTimeout(() => [Navigator.navTo("ForgetPassword")], 0);
    // Navigator.showLoading("Cho cmm luon");
  };
  render() {
    return (
      <Col flex={1} style={styles.container}>
        <Col flex={8} style={styles.top}>
          <Col flex={3}>
            <Row flex={2} style={styles.row} alignVertical="flex-end">
              <Image
                source={Assets.images.logo}
                style={styles.rowLogo}
                resizeMethod="auto"
              />
              <Text style={styles.rowText}>{Strings.Login.jellyCity}</Text>
            </Row>
            <Text style={styles.textSignIn}>{Strings.Login.signIn}</Text>
            <Text style={styles.textWellcome}>{Strings.Login.wellcome}</Text>
          </Col>

          <Col flex={4} alignVertical="center">
            <Col flex={3} alignVertical="center">
              <Text style={styles.titleEmail}>{Strings.Login.titleEmail}</Text>
              <Row flex={1 / 3} style={styles.rowEmail} alignVertical="center">
                <TextInput
                  style={styles.textInputEmail}
                  underlineWidth={0}
                  onChangeText={email =>
                    this.setState({
                      email,
                      showCheck: email == "trung" ? true : false
                    })
                  }
                  // underlineColor="#707070"
                />
                <Image
                  source={Assets.images.checked}
                  style={[
                    styles.checked,
                    this.state.showCheck ? {} : styles.hide
                  ]}
                />
              </Row>
            </Col>
            <Col flex={3} alignVertical="center">
              <Text style={styles.titlePassword}>
                {Strings.Login.titlePassword}
              </Text>
              <TextInput style={styles.textInputPassword} underlineWidth={1} />
              <TouchableOpacity onPress={() => this.onForgetPassword()}>
                <Text style={styles.forgetPassword}>
                  {Strings.Login.forgetPassword}
                </Text>
              </TouchableOpacity>
            </Col>
          </Col>
        </Col>
        <Col flex={3} style={styles.bot} alignVertical="center">
          <Col style={styles.buttonNext} onPress={() => this.onLogin()}>
            <Text style={styles.textNext}>{Strings.Login.next}</Text>
          </Col>
        </Col>
      </Col>

      // <View style={StyleSheet.absoluteFill}>
      //   <Header title="Login Page" />
      //   <Button
      //     text="Show toast"
      //     onPress={() => Navigator.showToast("This is a header", "This is a message", "Error")}
      //   />
      //   <Button
      //     text="Show alert confirm"
      //     onPress={() => Navigator.showAlert("", "This is a message", () => {}, () => {})}
      //   />
      //   <Button text="Show alert" onPress={() => Navigator.showAlert("", "This is a message")} />
      //   <Button
      //     text="Show loading"
      //     onPress={() => {
      //       Navigator.showLoading();
      //       setTimeout(() => Navigator.hideLoading(), 3000);
      //     }}
      //   />
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: width / 15
  },
  top: {
    flex: 8
  },
  bot: {
    flex: 2
  },
  buttonNext: {
    borderRadius: 5,
    backgroundColor: "#91C95B",
    width: width * (13 / 15),
    flex: 1 / 3,
    alignItems: "center",
    justifyContent: "center"
  },
  textNext: {
    color: "white",
    fontSize: 15,
    textAlign: "center"
  },
  rowLogo: {},
  row: {},
  rowText: {
    color: "#006EE2",
    fontSize: 20,
    marginLeft: 21
  },
  textWellcome: {
    flex: 1,
    fontSize: 21
  },
  textSignIn: {
    flex: 3,
    color: "#FE4F70",
    fontSize: 68
  },
  titleEmail: {
    flex: 1 / 5,
    fontSize: 18,
    fontWeight: "bold",
    alignItems: "flex-end"
  },
  titlePassword: {
    flex: 1 / 4,
    fontSize: 18,
    fontWeight: "bold",
    alignItems: "flex-end",
    marginTop: 5
  },
  rowEmail: {
    borderBottomWidth: 1,
    // borderBottomColor: "#707070",
    flexDirection: "row"
  },
  textInputEmail: {
    borderWidth: 0,
    flex: 9
  },
  textInputPassword: {
    flex: 1 / 3
    // textContentType: "password"
  },
  forgetPassword: {
    // flex: 1 / 2,
    fontSize: 11,
    color: "#6DB4FF",
    marginTop: 8,
    textAlign: "right"
  },
  checked: {},
  hide: {
    width: 0,
    height: 0
  }
});

export default Login;
