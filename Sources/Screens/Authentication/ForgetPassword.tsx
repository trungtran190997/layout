import React from "react";
import {
  Image,
  Text,
  TouchableOpacity,
  Dimensions
  // TextInputComponent
} from "react-native";
import { StyleSheet } from "rn-components";
import { Col, Row, TextInput } from "rn-components";
import { Strings } from "@Localization";
import Assets from "@Assets";
import { Navigator } from "@Navigation";
import Home from "../Home/Home";

const { width, height } = Dimensions.get("screen");
// type State = {};
// type Props = {};
class ForgetPassword extends React.Component {
  onNewPassword = () => {
    Navigator.showLoading();
    setTimeout(() => [Navigator.navTo("Home"), Navigator.hideLoading()], 500);
  };
  render() {
    return (
      <Col flex={1} style={styles.container}>
        <Col flex={8} style={styles.top}>
          <Col flex={3}>
            <Row flex={2} style={styles.row} alignVertical="flex-end">
              <Image
                source={Assets.images.logo}
                style={styles.rowLogo}
                resizeMethod="auto"
              />
              <Text style={styles.rowText}>
                {Strings.NewPassword.jellyCity}
              </Text>
            </Row>
            <Text style={styles.textSignIn}>{Strings.NewPassword.signIn}</Text>
          </Col>

          <Col flex={6} alignVertical="center">
            <Col flex={3} alignVertical="center">
              <Text style={styles.textOTP}>{Strings.NewPassword.OTP}</Text>
              <Row
                flex={2}
                alignHorizontal="space-around"
                alignVertical="center"
              >
                <Col
                  flex={1 / 8}
                  alignHorizontal="center"
                  style={styles.colOTP}
                >
                  <TextInput
                    style={styles.inputOTP}
                    textContentType="telephoneNumber"
                  />
                </Col>
                <Col
                  flex={1 / 8}
                  alignHorizontal="center"
                  style={styles.colOTP}
                >
                  <TextInput
                    style={styles.inputOTP}
                    textContentType="telephoneNumber"
                  />
                </Col>
                <Col
                  flex={1 / 8}
                  alignHorizontal="center"
                  style={styles.colOTP}
                >
                  <TextInput
                    style={styles.inputOTP}
                    textContentType="telephoneNumber"
                  />
                </Col>
                <Col
                  flex={1 / 8}
                  alignHorizontal="center"
                  style={styles.colOTP}
                >
                  <TextInput
                    style={styles.inputOTP}
                    textContentType="telephoneNumber"
                  />
                </Col>
              </Row>
            </Col>
            <Col flex={2} alignVertical="center">
              <Text style={styles.titleEmail}>
                {Strings.NewPassword.newPassword}
              </Text>
              <Row flex={1 / 3} style={styles.rowEmail} alignVertical="center">
                <TextInput
                  style={styles.textInputEmail}
                  underlineWidth={0}
                  textContentType="password"
                  // onChangeText={email => this.setState({ email })}
                  // underlineColor="#707070"
                />
              </Row>
            </Col>
            <Col flex={2} alignVertical="center">
              <Text style={styles.titlePassword}>
                {Strings.NewPassword.confirm}
              </Text>
              <TextInput
                style={styles.textInputPassword}
                underlineWidth={1}
                textContentType="password"
              />
            </Col>
          </Col>
        </Col>
        <Col flex={2} style={styles.bot} alignVertical="center">
          <Col style={styles.buttonNext} onPress={() => this.onNewPassword()}>
            <Text style={styles.textNext}>
              {Strings.NewPassword.buttonNext}
            </Text>
          </Col>
        </Col>
      </Col>

      // <View style={StyleSheet.absoluteFill}>
      //   <Header title="Login Page" />
      //   <Button
      //     text="Show toast"
      //     onPress={() => Navigator.showToast("This is a header", "This is a message", "Error")}
      //   />
      //   <Button
      //     text="Show alert confirm"
      //     onPress={() => Navigator.showAlert("", "This is a message", () => {}, () => {})}
      //   />
      //   <Button text="Show alert" onPress={() => Navigator.showAlert("", "This is a message")} />
      //   <Button
      //     text="Show loading"
      //     onPress={() => {
      //       Navigator.showLoading();
      //       setTimeout(() => Navigator.hideLoading(), 3000);
      //     }}
      //   />
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: width / 15
  },
  top: {
    flex: 8
  },
  bot: {
    flex: 2
  },
  buttonNext: {
    borderRadius: 5,
    backgroundColor: "#91C95B",
    width: width * (13 / 15),
    flex: 1 / 3,
    alignItems: "center",
    justifyContent: "center"
  },
  textNext: {
    color: "white",
    fontSize: 15,
    textAlign: "center"
  },
  rowLogo: {},
  row: {},
  rowText: {
    color: "#006EE2",
    fontSize: 20,
    marginLeft: 21
  },
  textWellcome: {
    flex: 1,
    fontSize: 21
  },
  textSignIn: {
    flex: 3,
    color: "#FE4F70",
    fontSize: 68
  },
  titleEmail: {
    flex: 1 / 4,
    fontSize: 18,
    fontWeight: "bold",
    alignItems: "flex-end"
  },
  titlePassword: {
    flex: 1 / 4,
    fontSize: 18,
    fontWeight: "bold",
    alignItems: "flex-end"
  },
  rowEmail: {
    borderBottomWidth: 1,
    // borderBottomColor: "#707070",
    flexDirection: "row"
  },
  textInputEmail: {
    borderWidth: 0,
    flex: 9
  },
  textInputPassword: {
    flex: 1 / 3
    // textContentType: "password"
  },
  forgetPassword: {
    // flex: 1 / 2,
    fontSize: 11,
    color: "#6DB4FF",
    marginTop: 8,
    textAlign: "right"
  },
  checked: {},
  textOTP: {
    flex: 1,
    fontWeight: "bold",
    fontSize: 18,
    textAlignVertical: "bottom"
  },
  inputOTP: {
    borderWidth: 0,
    borderBottomWidth: 0,
    textAlign: "center",
    borderColor: "#707070"
  },
  colOTP: {
    borderWidth: 1,
    borderColor: "#707070",
    borderRadius: 3
  }
});
export default ForgetPassword;
