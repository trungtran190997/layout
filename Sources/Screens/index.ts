export { default as Start } from "./Start/Start";
export { default as Login } from "./Authentication/Login";
export { default as Home } from "./Home/Home";
export { default as HomeScreen } from "./Home/HomeScreen";
export { default as EditName } from "./Authentication/EditName";
export { default as Homepage1 } from "./Home/Homepage1";
export { default as ForgetPassword } from "./Authentication/ForgetPassword";
