const EditName = {
  title: "Edit Name",
  titleOldName: "Old Name",
  oldName: "Jamie Huynh",
  titleNewName: "New Name"
};

const Login = {
  next: "NEXT",
  jellyCity: "JELLY CITY",
  signIn: "SIGN IN",
  wellcome: "Wellcome home Friend!",
  titleEmail: "Email",
  titlePassword: "Password",
  hideEmail: "Enter Email",
  hidePassword: "Enter Password",
  forgetPassword: "Forget password?"
};

const Home = {
  hi: "Hi ",
  wellcome: "Wellcome to your home!"
};

const NewPassword = {
  jellyCity: "JELLY CITY",
  signIn: "SIGN IN",
  OTP: "OTP Code",
  newPassword: "New Password",
  confirm: "Confirm New Password",
  buttonNext: "NEXT"
};

const Homepage1 = {
  bookNow: "Book Now",
  wellcome: "Wellcome",
  goodMorning: "Good Morning",
  fri: "Fri, 4th"
};

const AvaiableFacility = {
  title: "AVAIABLE FACILITY",
  all: "All",
  titleSwimming: "Swimming Pool",
  titleParking: "Parking Lot",
  titleBBQ: "BBQ",
  contentSwimming: "Swimming pool on the 8th floor",
  contentParking: "Send car in the basement",
  contentBBQ: "Center park at the building"
};
const Payment = {
  payment: "PAYMENT",
  all: "All",
  total: "Total",
  parkingLot: "Parking Lot",
  payNow: "Pay Now",
  contentTotal: "(Managerment fee, Parking lot)",
  contentParkingLot: "1 motobike",
  priceTotal: "1,134,550",
  priceParkingLot: "200,000",
  vnd: "(VND)",
  dayLeft: "Day Left",
  numDayLeft: 1
};
const Promotion = {
  promotion: "PROMOTION",
  all: "All",
  percent: "50%",
  saleOff: "Sale Off",
  content: "Seafood Buffet of",
  address: "Hoang Yen Restaurant",
  vailid: "VAILID UNTIL 30:10:01 IN ORDER"
};
const TabBar = {
  home: "Home",
  facility: "Facility",
  payment: "Payment"
};
export default {
  AvaiableFacility,
  NewPassword,
  EditName,
  Login,
  Home,
  Homepage1,
  Payment,
  Promotion,
  TabBar
};
