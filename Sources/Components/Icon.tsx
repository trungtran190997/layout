import React from "react";
import { Image } from "react-native";
import { Row } from "rn-components";
import Assest from "@Assets";

type Props = {
  icon: any;
};
class Icon extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }
  render() {
    return (
      <Row flex={1} alignHorizontal="center" alignVertical="center">
        <Image source={this.props.icon} />
      </Row>
    );
  }
}

export default Icon;
