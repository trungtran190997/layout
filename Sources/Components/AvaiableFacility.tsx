import React from "react";
import {
  FlatList,
  Text,
  Image,
  Dimensions,
  TouchableOpacity
} from "react-native";
import { Col, Row, StyleSheet, Touchable } from "rn-components";
import Assets from "@Assets";
import { Strings } from "@Localization";

const { width, height } = Dimensions.get("screen");
type Props = {
  item: any;
};

class AvaiableFacility extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }
  onRenderItemAvaiable = (item: any) => {
    return (
      <Row
        alignVertical="center"
        alignHorizontal="space-between"
        style={styles.container}
      >
        <Row flex={8 / 10}>
          <Col flex={2} alignHorizontal="flex-end">
            <Image source={item.img} style={styles.img} />
          </Col>
          <Col flex={8} alignVertical="center" style={styles.colText}>
            <Text style={styles.textTitleItem}>{item.title}</Text>
            <Text style={styles.textContent}>{item.content}</Text>
          </Col>
        </Row>
        <TouchableOpacity>
          <Image source={Assets.images.next} style={styles.imgNext} />
        </TouchableOpacity>
      </Row>
    );
  };

  onRenderSeparator = () => {
    return <Row style={styles.separator} flex={1} />;
  };

  render() {
    const dataAvaiable = [
      {
        img: Assets.images.swimming,
        title: Strings.AvaiableFacility.titleSwimming,
        content: Strings.AvaiableFacility.contentSwimming
      },
      {
        img: Assets.images.car,
        title: Strings.AvaiableFacility.titleParking,
        content: Strings.AvaiableFacility.contentParking
      },
      {
        img: Assets.images.BBQ,
        title: Strings.AvaiableFacility.titleBBQ,
        content: Strings.AvaiableFacility.contentBBQ
      }
    ];

    return (
      <Col style={styles.container}>
        <Row alignHorizontal="space-between" alignVertical="center">
          <Row alignVertical="center">
            <Image source={Assets.images.imgTitle} />
            <Text style={styles.textTitle}>
              {Strings.AvaiableFacility.title}
            </Text>
          </Row>
          <Text style={styles.textAll}>{Strings.AvaiableFacility.all}</Text>
        </Row>
        <FlatList
          ItemSeparatorComponent={() => this.onRenderSeparator()}
          data={dataAvaiable}
          renderItem={({ item }) => this.onRenderItemAvaiable(item)}
          //   renderItem={({ item }) => <Text>{item.key}</Text>}
        />
      </Col>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: width / 20,
    backgroundColor: "white"
  },
  separatorFlatList: {
    opacity: 20,
    width,
    backgroundColor: "#707070",
    height: 6
  },
  itemPayMent: {
    marginLeft: 5
  },
  textTitlePayMent: {
    fontSize: 23,
    fontWeight: "bold"
  },
  textContentPayment: {
    fontSize: 15,
    color: "#D9D9D9"
  },
  textPrice: {
    fontSize: 26,
    color: "#FE4F70"
  },
  textVND: {
    fontSize: 16,
    color: "#FE4F70"
  },
  textPayNow: {
    fontSize: 15,
    color: "white",
    fontWeight: "800"
  },
  textAll: {
    fontSize: 15,
    opacity: 70
  },
  textDayLeft: {
    fontSize: 15
  },

  separator: {
    height: 1,
    backgroundColor: "#24CA99",
    opacity: 80
  },
  textTitle: {
    fontSize: 23,
    color: "#909090",
    fontWeight: "bold",
    opacity: 70,
    marginLeft: 11
  },
  img: {
    // alignSelf: "flex-end",
    // alignContent: "flex-end"
    // flex: 3 / 8
    // marginRight: 38
    // backgroundColor: "red"
  },
  imgNext: {
    // flex: 1
  },
  textTitleItem: {
    fontSize: 20,
    fontWeight: "bold"
  },
  textContent: {
    fontWeight: "100",
    fontSize: 13
  },
  colText: {
    // marginRight: 40
    marginLeft: 20
  }
});

export default AvaiableFacility;
