import React from "react";
import {
  FlatList,
  Text,
  Image,
  Dimensions,
  Button,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { Col, Row, StyleSheet } from "rn-components";
import Assets from "@Assets";
import { Strings } from "@Localization";
// import { Payment } from "@Components";

const { width, height } = Dimensions.get("screen");
type Props = {
  item: any;
};

class Payment extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  onRenderPayMent = (item: any) => {
    return (
      <Row alignHorizontal="space-between" style={styles.itemPayMent}>
        <Col alignVertical="space-around" flex={1}>
          <Col flex={1}>
            <Text style={styles.textTitlePayMent}>{item.title}</Text>
          </Col>
          <Col flex={2} alignVertical="space-between">
            <Row flex={1} alignVertical="center">
              <Text style={styles.textPrice}>{item.price}</Text>
              <Text style={styles.textVND}>{Strings.Payment.vnd}</Text>
            </Row>
            <Text style={styles.textContentPayment}>{item.content}</Text>
          </Col>
        </Col>
        <Col alignVertical="space-around" flex={1}>
          <Row
            alignHorizontal="flex-end"
            alignVertical="center"
            style={item.img ? styles.show : styles.hide}
          >
            <Col alignSelf="center">
              <ImageBackground
                source={item.img}
                style={styles.imgDayLeft}
                resizeMode="cover"
              >
                <Text style={styles.numDayLeft}>{item.numDayLeft}</Text>
              </ImageBackground>
            </Col>

            <Text style={styles.textDayLeft}>{item.textDayLeft}</Text>
          </Row>
          <Row alignHorizontal="flex-end">
            <TouchableOpacity>
              <Image source={Assets.images.payNow} />
            </TouchableOpacity>
          </Row>
        </Col>
      </Row>
    );
  };

  onRenderSeparatorPayment = () => {
    return (
      <Col style={styles.separator}>
        <Image source={Assets.images.line} />
      </Col>
    );
  };
  render() {
    const dataPayment = [
      {
        title: Strings.Payment.total,
        content: Strings.Payment.contentTotal,
        price: Strings.Payment.priceTotal,
        textPayNow: Strings.Payment.payNow
      },
      {
        title: Strings.Payment.parkingLot,
        content: Strings.Payment.contentParkingLot,
        price: Strings.Payment.priceParkingLot,
        img: Assets.images.dayLeft,
        textDayLeft: Strings.Payment.dayLeft,
        numDayLeft: Strings.Payment.numDayLeft,
        textPayNow: Strings.Payment.payNow
      }
    ];

    return (
      <Col style={styles.container}>
        <Row alignHorizontal="space-between" alignVertical="center">
          <Row alignVertical="center">
            <Image source={Assets.images.imgTitle} />
            <Text style={styles.textTitle}>{Strings.Payment.payment}</Text>
          </Row>
          <Text style={styles.textAll}>{Strings.Payment.all}</Text>
        </Row>
        <FlatList
          data={dataPayment}
          renderItem={({ item }) => this.onRenderPayMent(item)}
          ItemSeparatorComponent={() => this.onRenderSeparatorPayment()}
        />
      </Col>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 7,
    padding: width / 20,
    backgroundColor: "white"
  },
  numDayLeft: {
    fontSize: 17,
    color: "#E3002E"
  },
  imgDayLeft: {
    width: undefined,
    height: undefined
  },
  separatorFlatList: {
    opacity: 20,
    width,
    backgroundColor: "#707070",
    height: 6
  },
  itemPayMent: {
    marginLeft: 5
  },
  textTitlePayMent: {
    fontSize: 23,

    color: "#000000"
  },
  hide: {
    display: "none"
  },
  show: {
    display: "flex"
  },
  textContentPayment: {
    fontSize: 15,
    color: "#D9D9D9"
  },
  textPrice: {
    fontSize: 26,
    color: "#FE4F70"
  },
  textVND: {
    fontSize: 16,
    color: "#FE4F70"
  },
  textPayNow: {
    fontSize: 15,
    color: "white",
    fontWeight: "800"
  },
  backgroundPayNow: {
    backgroundColor: "#3C96FF",
    borderRadius: 4,
    // borderWidth: 1,
    borderColor: "#707070"
  },
  textAll: {
    fontSize: 15,
    opacity: 70
  },
  textDayLeft: {
    // flex: 2,
    fontSize: 15,
    marginLeft: 5,
    color: "#231F20"
  },

  separator: {
    marginTop: 11,
    marginBottom: 11
  },
  textTitle: {
    fontSize: 23,
    color: "#909090",
    fontWeight: "bold",
    opacity: 70,
    marginLeft: 11
  },
  img: {
    // alignSelf: "flex-end",
    // alignContent: "flex-end"
    // flex: 3 / 8
    // marginRight: 38
    // backgroundColor: "red"
  },
  imgNext: {
    // flex: 1
  },
  textTitleItem: {
    fontSize: 20,
    fontWeight: "bold"
  },
  textContent: {
    fontWeight: "100",
    fontSize: 13
  },
  colText: {
    // marginRight: 40
    marginLeft: 20
  }
});

export default Payment;
