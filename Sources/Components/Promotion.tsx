import React from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  Image,
  ImageBackground
} from "react-native";
import { Col, Row } from "rn-components";
import { Strings } from "@Localization";
import Assets from "@Assets";

const { width, height } = Dimensions.get("screen");
class Promotion extends React.Component {
  render() {
    return (
      <Col flex={1} style={styles.container}>
        <Row flex={1} alignHorizontal="space-between" alignVertical="center">
          <Row alignVertical="center">
            <Image source={Assets.images.imgTitle} />
            <Text style={styles.textPromotion}>
              {Strings.Promotion.promotion}
            </Text>
          </Row>
          <Text style={styles.textAll}>{Strings.Promotion.all}</Text>
        </Row>
        <Col flex={7} alignHorizontal="center" alignVertical="center">
          <ImageBackground
            //   style={styles.backgroundSaleOff}
            source={Assets.images.backgroundSaleOff}
            style={styles.backgroundSaleOff}
            resizeMode="stretch"

            // resizeMethod="scale"
          >
            <Col alignVertical="space-between">
              <Col flex={5} alignVertical="space-around" style={styles.colSale}>
                <Row alignVertical="flex-end">
                  <Text style={styles.textPercent}>
                    {Strings.Promotion.percent}
                  </Text>
                  <Text style={styles.textSaleOff}>
                    {Strings.Promotion.saleOff}
                  </Text>
                </Row>
                <Text style={styles.textAddress}>
                  {Strings.Promotion.content}
                </Text>
                <Text style={styles.textAddress}>
                  {Strings.Promotion.address}
                </Text>
              </Col>
              <Row
                flex={1}
                style={styles.bgVailid}
                alignHorizontal="center"
                // source={Assets.images.bgVailid}
                // resizeMode="stretch"
                // resizeMethod="scale"
              >
                <Text style={styles.textVailid}>
                  {Strings.Promotion.vailid}
                </Text>
              </Row>
            </Col>
          </ImageBackground>
        </Col>
      </Col>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 7,
    backgroundColor: "white",
    padding: width / 20
  },
  textPromotion: {
    fontSize: 23,
    color: "#909090",
    fontWeight: "bold",
    opacity: 70,
    marginLeft: 11
  },
  textAll: {
    fontSize: 15,
    opacity: 70
  },
  colSale: {
    paddingLeft: 30,
    padding: 12
  },
  backgroundSaleOff: {
    height: undefined,
    width: undefined,
    flex: 1
  },
  bgVailid: {
    backgroundColor: "#4C9896"
  },
  textPercent: {
    fontSize: 36,
    fontWeight: "bold",
    color: "white",
    marginRight: 7
  },
  textSaleOff: {
    fontSize: 22,

    color: "white"
  },
  textAddress: {
    fontSize: 17,

    color: "white"
  },
  textVailid: {
    fontSize: 14,
    color: "white"
  }
});

export default Promotion;
