export { default as AvaiableFacility } from "./AvaiableFacility";
export { default as Payment } from "./Payment";
export { default as Promotion } from "./Promotion";
export { default as Icon } from "./Icon";
// export { default as Promotion } from "./Promotion";
